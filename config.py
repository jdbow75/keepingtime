# Per app config
from os import environ

tpl_dir='templates'

if environ.get('SERVER_SOFTWARE'):
    on_development = environ['SERVER_SOFTWARE'].startswith('Devel')
else:
    on_development = False

#to generate secrets:
#import base64, os
#r=base64.b64encode(os.urandom(66)).decode()
#print('\\\n'.join((repr(r[:44]),repr(r[44:]))))

session_secret = 'ZI2Zs6w+tuzh4jZLzphZifAZiEsi6cASgIqs9Gy4H4Uv'\
                 'TXrnesj6weALUY038IBYcImZci41sebFyeA7zg6ilhde'
session_cookie_name = 'DSg89'

secure_session_secret = 'qpeaFSf4TX5iX786AwbyncrtF/1YGY+UeXt7Y0DZJ/u2'\
                        '2TUglcxyd7cPG0Bt1klPom2lSKoCDHRBZ6j0XWlKILKy'
secure_session_cookie_name = 'A3o23'

id_secret = 'CqX66u53hedrIha4+Lk32eHdXIEfSqw/u7uUbnj9+ocg'\
            'qNg9afF7Sl+R8eTLzfOu8fB8zd/9wvBDNoq2sEAE69zi'
id_cookie_name = '98atW'

