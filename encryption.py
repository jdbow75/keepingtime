import base64
import hashlib
import hmac
import pickle
import zlib
from Crypto.Cipher import AES
from Crypto.Protocol.KDF import PBKDF2
import Crypto.Random

try:
    compare = hmac.compare_digest
except AttributeError:
    try:
        from itertools import izip as zip
    except ImportError:
        pass
    def compare(a, b):
        if len(a) != len(b):
            return False
        rv = 0
        for x, y in zip(bytearray(a), bytearray(b)):
            rv |= x ^ y
        return rv == 0

ciphertext_prefix = '___ciphertext___'.encode()


def prf(p,s):
    return hmac.new(p,s,hashlib.sha256).digest()

def encrypt(data, password):
    salt = Crypto.Random.new().read(16)
    data = zlib.compress(pickle.dumps(data))
    #hashed = hmac.new(salt, password, hashlib.sha384).digest()
    hashed = PBKDF2(password, salt, 48, 1000, prf)
    key = hashed[:16]
    iv = hashed[16:32]
    mac_key = hashed[32:]
    pad_val = 16 - (len(data) % 16)
    pad = chr(pad_val).encode()*pad_val
    data += pad

    cipher = AES.new(key, AES.MODE_CBC, iv)
    ciphertext = cipher.encrypt(data)
    output = ''.encode().join((ciphertext_prefix,
                      hmac.new(mac_key, ciphertext, hashlib.sha256).digest(),
                      salt,
                      ciphertext))
    return base64.b64encode(output)


def decrypt(data, password):
    if not data:
        return False
    data = base64.b64decode(data)
    if not data.startswith(ciphertext_prefix):
        return False
    received_mac = data[16:48]
    salt = data[48:64]
    data = data[64:]
    #hashed = hmac.new(salt, password, hashlib.sha384).digest()
    hashed = PBKDF2(password, salt, 48, 1000, prf)
    key = hashed[:16]
    iv = hashed[16:32]
    mac_key = hashed[32:]

    cipher = AES.new(key, AES.MODE_CBC, iv)

    if not compare(received_mac, 
                   hmac.new(mac_key, data, hashlib.sha256).digest()):
        return False

    plaintext = cipher.decrypt(data)
    output = zlib.decompress(plaintext.rstrip(plaintext[-1:]))
    return pickle.loads(output)
