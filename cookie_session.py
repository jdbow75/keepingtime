from collections import MutableMapping
import datetime
import webob
from encryption import encrypt, decrypt

class Session(MutableMapping):
    def __init__(self, secret, data=None,
            lifetime=datetime.timedelta(30), secure=False):
        self.deleted = False
        self.payload = data
        self.secret = secret
        self.lifetime = lifetime
        self.changed = False
        self.new = True
        self.secure = secure

    def data():
        doc = "The underlying dictionary"
        def fget(self):
            try:
                return(self._data)
            except AttributeError:
                data = decrypt(self.payload, self.secret)
                now = datetime.datetime.utcnow()
                if data and (data.get('expires',datetime.datetime.min) > now):
                    self._data = data
                    self.new = False
                else:
                    self.clear()
                return(self._data)
        return locals()
    data = property(**data())

    def clear(self):
        now = datetime.datetime.utcnow()
        self._data = {
            'expires': now + self.lifetime,
        }
        self.new = True
        self.changed = True

    def delete(self):
        self.deleted = True

    def reset(self):
        del self._data
        self.changed = False

    def save(self):
        if self.changed:
            data = encrypt(self._data, self.secret)
            if len(data) > 4000:
                self.changed = False
            else:
                self.payload = data
        return self.changed

    def __contains__(self, key):
        return self.data.__contains__(key)

    def __delitem__(self, key):
        self.data.__delitem__(key)
        self.changed = True

    def __getitem__(self, key):
        return self.data[key]

    def __iter__(self):
        return self.data.iterkeys()

    def __len__(self):
        return len(self.data)

    def __repr__(self):
        return repr(self.data)

    def __setitem__(self, key, value):
        self.changed = True
        self.data[key] = value

class SessionMiddleware(object):
    def __init__(self, app, secret,
                 lifetime=datetime.timedelta(14),
                 cookie_path='/',
                 cookie_name='session_data',
                 secure=True):
        self.app = app
        self.cookie_path = cookie_path
        self.cookie_name = cookie_name
        self.lifetime = lifetime
        self.secret = secret
        self.secure = secure

    def __call__(self, environ, start_response):
        req = webob.Request(environ)
        data=req.cookies.get(self.cookie_name)
        session = Session(lifetime=self.lifetime, secret=self.secret, data=data)
        req.environ['session'] = session
        resp = req.get_response(self.app)
        if session.deleted:
            resp.delete_cookie(self.cookie_name, self.cookie_path)
        elif session.save() and (len(session.payload) <= 4000):
            cookie_params = dict(key=self.cookie_name, value=session.payload,
                            path=self.cookie_path)
            if session.new:
                cookie_params.update(dict(expires=session['expires'],
                                          httponly=True,
                                          secure=session.secure))
            resp.set_cookie(**cookie_params)
        return resp(environ, start_response)
