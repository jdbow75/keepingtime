from base64 import urlsafe_b64decode
from urlparse import parse_qs
import json

def app(environ, start_response):
    params = parse_qs(environ['QUERY_STRING'])
    if params.get('state'):
        state = json.loads(urlsafe_b64decode(params['state'][0]))
        url = state['redirect_uri'] + '?' + environ['QUERY_STRING']
        start_response('303 See Other', [('Location', url.encode())])
        return []
    else:
        start_response('404 Not Found', [('content-type', 'text/plain')])
        return ['This is not the page you are looking for, unless in fact you are looking for oh for.\nGet it?\nOh, I am the funniest web server in the land.']
