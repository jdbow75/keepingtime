#!/usr/bin/env python3.4

try:
    import on_development
    on_development = True
except ImportError:
    on_development = False

from bottle import app, jinja2_view as view, request, route, run
import config

from cookie_session import SessionMiddleware
app = SessionMiddleware(app(), config.session_secret, secure=not on_development)


@route('/hello/:name')
@view('index')
def index(name='World'):
    session = request.environ.get('session')
    for i in range(10):
        moniker = 'name' + str(i)
        if session.get(moniker):
            continue
        else:
            session[moniker] = name
            break
    return {'name': name, 'session': session}


# Change for deployment
run(app=app, host='localhost', port=31705, debug=on_development, reloader=on_development, server='cherrypy')
