import base64
import config
import datetime
from .encryption import str_cmp
import hmac, hashlib
import pickle
import time
from urlparse import parse_qs, parse_qsl
import json
from urllib2 import urlopen, Request
from google.appengine.api.urlfetch import fetch
from urllib import quote, urlencode
import webob
from webob.exc import *
import oauth_services
try:
    from Crypto.Random import new, random
    pool = new()
    randbytes = pool.read
except ImportError:
    from random import SystemRandom
    random = SystemRandom()
    from os import urandom as randbytes

from .cookie_session import SessionMiddleware

alphanumeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

def auth(environ, response):
    req = webob.Request(environ)
    res = webob.Response()
    body = res.body_file
    body.write("""<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
 "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, minimum-scale=0.5, maximum-scale=3.0,
      user-scalable=yes, target-densitydpi=device-dpi"/>
    <title>What to give</title>
    <link rel="icon" type="image/vnd.microsoft.icon" href="/static/listsicon.ico">
    <link rel="stylesheet" href="/static/auth.css" type="text/css">
</head>
<body>
    """)
    try:
        user_data = base64.b64decode(req.str_cookies[config.id_cookie_name])
        expire_pickle = user_data[112:]
        expires = pickle.loads(expire_pickle)
        salt = user_data[:16]
        hashed = hashlib.sha512(config.id_secret).digest()
        sig_key = hmac.new(salt, hashed[32:], hashlib.sha256).digest()
        received_sig = user_data[16:64]
        computed_sig = hmac.new(sig_key,user_data[64:],hashlib.sha384).digest()
        if (datetime.datetime.utcnow() > expires or
                not str_cmp(received_sig, computed_sig)):
            res.delete_cookie(config.id_cookie_name, path='/auth')
            raise Exception
        user_id = user_data[64:112]
        environ['session']['user_id'] = user_id

    except:
        body.write("""
<ul class="clear">
<li><a href="/auth/google"><div class="icon ggl"></div>Sign in with Google</a></li>
<li><a href="/auth/facebook"><div class="icon fb"></div>Sign in with Facebook</a></li>
<li><a href="/auth/live"><div class="icon lv"></div>Sign in with Microsoft Live</a></li>
<li><a href="/auth/twitter"><div class="icon tw"></div>Sign in with Twitter</a></li>
<li><a href="/auth/linkedin"><div class="icon li"></div>Sign in with LinkedIn</a></li>
<li><a href="/auth/paypal"><div class="icon pp"></div>Sign in with Paypal</a></li>
<li><a href="/auth/yahoo"><div class="icon yh"></div>Sign in with Yahoo</a></li>
</ul>
        """)

    body.write("""
</body>
</html>""")
    return res(environ, response)

def oauth1_request(url, oauth_params, consumer_secret, token_secret='',
                   method='POST'):
    oauth_params = oauth_params.copy()
    oauth_params.update({
        'oauth_nonce': ''.join(random.choice(alphanumeric) for i in range(50)),
        'oauth_signature_method': 'HMAC-SHA1',
        'oauth_timestamp': str(int(time.time())),
        'oauth_version': '1.0',
    })
    sig_base = '&'.join((method,quote(url, '~'), quote(urlencode(
        sorted(oauth_params.iteritems())), '')))
    sig_key = '&'.join((quote(consumer_secret, ''),
        quote(token_secret, '')))
    oauth_params['oauth_signature'] = base64.b64encode(
            hmac.new(sig_key,sig_base,hashlib.sha1).digest())
    auth_header = 'OAuth '
    auth_header += ', '.join('{}="{}"'.format(quote(k, ''),quote(v, ''))
                             for k,v in sorted(oauth_params.iteritems()))
    resp = fetch(url, method=method,
                 headers={'Authorization': auth_header,
                          'x-li-format': 'json'},
                 validate_certificate=True)
    if resp.status_code == 200:
        content = resp.content
    else:
        content = ''

    return content

def service(environ, response):
    user_id = None
    req = webob.Request(environ)
    session = req.environ['session']
    service = req.urlvars['service']
    params = oauth_services.params[service]
    if params.get('oauth_version') == '1.0':
        oauth_params = session.get('oauth_params',{})
        if req.GET.get('oauth_token'):
            if req.GET['oauth_token'] != oauth_params['oauth_token']:
                res = HTTPForbidden('Something scary is happening.')
                return res(req.environ, response)

            del oauth_params['oauth_callback']
            oauth_params['oauth_verifier'] = req.GET['oauth_verifier']
            result = oauth1_request(params['access_token_url'],
                                    oauth_params=oauth_params,
                                    consumer_secret=params['consumer_secret'],
                                    token_secret=oauth_params.pop('oauth_token_secret'),
                                    )
            #result = dict((k,v[0]) for k,v in parse_qs(result).iteritems())
            result = dict(parse_qsl(result))
            if params.get('info_url'):
                del oauth_params['oauth_verifier']
                oauth_params['oauth_token'] = result['oauth_token']
                result = oauth1_request(params['info_url'],
                                        oauth_params=oauth_params,
                                        consumer_secret=params['consumer_secret'],
                                        token_secret=result['oauth_token_secret'],
                                        method='GET',
                                        )
                result=json.loads(result)
        else:
            oauth_params = {
                'oauth_callback': req.path_url,
                'oauth_consumer_key': params['consumer_key'],
            }
            result = oauth1_request(params['request_token_url'],
                                    oauth_params=oauth_params,
                                    consumer_secret=params['consumer_secret'],
                                    )
            result = dict((k,v[0]) for k,v in parse_qs(result).iteritems()
                    if k in ('oauth_token','oauth_token_secret'))
            oauth_params.update(result)
            session['oauth_params'] = oauth_params
            res = HTTPSeeOther(location=params['authorize_url'] + '?oauth_token='
                    + oauth_params['oauth_token'])
            return res(req.environ, response)
    else:
        auth_code = req.GET.get('code')
        if auth_code:
            state = json.loads(
                    base64.urlsafe_b64decode(req.GET['state'].encode()))
            if not str_cmp(state['fingerprint'], session['fingerprint']):
                raise Exception
            post = {
                'code': auth_code,
                'client_id': params['client_id'],
                'client_secret': params['client_secret'],
                'redirect_uri': params['redirect_uri'],
            }

            if params.get('grant_type'):
                post['grant_type'] = params['grant_type']
            http_req = Request(params['token_url'],
                               urlencode(post),
                               {"Accept" : "application/json"})
            fh = urlopen(http_req)
            result = fh.read()
            fh.close()
            if service == 'facebook':
                token_info = dict((k,v[0]) for k,v in parse_qs(result).items())
            else:
                token_info = json.loads(result)
            if token_info.get('error'):
                return HTTPNotFound(token_info['error'])(req.environ, response)
            if not token_info.get('access_token'):
                return HTTPNotFound('No access token')(req.environ, response)
            fh = urlopen(params['info_url'] +
                         token_info['access_token'])
            result=json.load(fh)
            fh.close()
        else:
            query = {
                'response_type': 'code',
                'client_id': params['client_id'],
                'redirect_uri': params['redirect_uri'],
                'scope': params['scope'],
            }
            fingerprint = randbytes(32)
            session['fingerprint'] = fingerprint
            state = {
                'redirect_uri': req.path_url,
                'fingerprint': fingerprint,
            }
            query['state'] = base64.urlsafe_b64encode(json.dumps(state))
            oauth_url = params['oauth_url'] + '?' + urlencode(query)
            res = HTTPSeeOther(location=oauth_url)
            return res(req.environ, response)

    user_id = params['get_id'](result)
    user = '@'.join((str(user_id),service))
    max_age = datetime.timedelta(14)
    expires = pickle.dumps(datetime.datetime.utcnow() + max_age)
    hashed = hashlib.sha512(config.id_secret).digest()
    id_key = hashed[:32]
    salt = randbytes(16)
    sig_key = hmac.new(salt, hashed[32:], hashlib.sha256).digest()
    user_id = (hmac.new(id_key,user,hashlib.sha384).digest() +
               expires)
    user_id = base64.b64encode(''.join((salt,
              hmac.new(sig_key,user_id,hashlib.sha384).digest(),
              user_id)))
    res = HTTPSeeOther(location='/auth')
    res.set_cookie(config.id_cookie_name, user_id,
                   httponly=True, max_age=max_age, path='/auth',
                   secure=not config.on_development)
    res.set_cookie('auth_service', service,
                   httponly=True, max_age=datetime.timedelta(7300), path='/auth',
                   secure=not config.on_development)
    return res(environ, response)

auth = SessionMiddleware(auth, secret=config.session_secret,
                         cookie_name=config.session_cookie_name,
                         cookie_path='/',
                         lifetime=datetime.timedelta(minutes=30))

service = SessionMiddleware(service, config.secure_session_secret,
                            cookie_name=config.secure_session_cookie_name,
                            cookie_path='/auth',
                            lifetime=datetime.timedelta(minutes=3),
                            secure=not config.on_development)
